
import App from './App';
import {createStore, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import React, {Component} from 'react'
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/saga'
import {Platform, StyleSheet, Text, View, Button} from 'react-native';

import {createStackNavigator,
    StackNavigator, 
    createBottomTabNavigator, 
    createAppContainer, 
    createDrawerNavigator,
    DrawerNavigator} 
    from 'react-navigation';

const defaultState = { value :0};

const reducer = (state = defaultState, action) => {
    if (action.type === 'UP')
        return { value: state.value + 1};
    if (action.type === 'DOWN')
        return {value: state.value - 1};
    return state;
};

let sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddleware));


class reduxsaga extends Component {
    render() {
        return (
            <View style = {{flex: 1}}>
                <Provider store = {store}>
                    <App />
                </Provider>
                <Button
                    onPress={() => {
                        this.props.navigation.navigate('sRA');
                    }}
                    title="Next"
                />
            </View>
            
        );
    };
};

sagaMiddleware.run(rootSaga);

export default reduxsaga;
