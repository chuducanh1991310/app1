/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';
import { LoginManager } from 'react-native-fbsdk'

import {createStackNavigator,
  StackNavigator, 
  createBottomTabNavigator, 
  createAppContainer, 
  createDrawerNavigator,
  DrawerNavigator} 
  from 'react-navigation';


type Props = {};
var self;
export default class fblogin extends Component<Props> {
  constructor (Props)
  {
    super(Props);
    self = this;
  }
  async facebooklogin() {
    try{
      LoginManager.setLoginBehavior('WEB_ONLY');
      let result = await LoginManager.logInWithReadPermissions(["public_profile"]);
      if (result.isCancelled){
        alert('login was cancelled');
      } else {
        self.props.navigation.navigate('saga');
      }
    } catch (error)
    {
      alert("Login fail with error: " + error);
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Button 
          title="Facebook login"
          color="#841584"
          onPress = {() => {
              this.facebooklogin();
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
