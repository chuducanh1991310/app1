/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator,
        StackNavigator, 
        createBottomTabNavigator, 
        createAppContainer, 
        createDrawerNavigator,
        DrawerNavigator} 
        from 'react-navigation';

import HomeClass from './screen/Home';
import LoginClass from './screen/login'
import fblogin from './screen/fblogin'
import reduxsaga from './screen/saga/index'
import soundRA from './screen/sound'
const Demonavigation = createDrawerNavigator({
  fb: {screen: fblogin, },
  saga: {screen: reduxsaga,},
  sRA: {screen: soundRA, },
});

const myApp = createAppContainer(Demonavigation);

export default myApp;
